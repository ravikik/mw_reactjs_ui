import axios from 'axios';

const GET_QUEUE_API_BASE_URL= "http://localhost:8080/api/v1/Apps/getAllAppl";
const POST_ADD_QUEUE_API_BASE_URL= "http://localhost:8080/api/v1/Apps/addAppl";




class JbossService {

    getJbossApp(){

        return axios.get(GET_QUEUE_API_BASE_URL);

     }

     AddJbossAppl(values){
        return axios.post(POST_ADD_QUEUE_API_BASE_URL,values);



     }
}

export default new JbossService()

