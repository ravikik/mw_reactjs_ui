import React, {useState} from 'react';
import * as mwIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import * as AiIcons from 'react-icons/ai';
import {Sidebardata} from '../Sidebar/Sidebardata';
import tmnaimg from '../../icons/tmna.jpg'; 
import './NavBar.css';
import '../../App.css';

function NavBar() {
    const [sidebar, setSidebar] = useState(true)
    const showSidebar = () => setSidebar(!sidebar)

    return (
        <>
        <div className="navbar">
        
            <Link to='#' className='menu-bars'>
              <mwIcons.FaBars className="bars" onClick={showSidebar} />
            </Link>
            < img className="tmnaimg" src={tmnaimg} />
           
           
            <div className="toolbar_logo"><a href="/">TMNA MIDDLEWARE INVENTORY</a></div>
            <div className="spacer"/>
            <div className="toolbar_navigation_lists">
              <ul> 
                  <li><a href="/getAllAppl">Jboss</a></li>
                  <li><a href="/weblogicappls">WebLogic</a></li>
                  <li><a href="/wasappls">WebSphere</a></li>
                  <li><a href="/iisapps">IIS</a></li>
                  <li><a href="/apacheurls">Apache</a></li>
                  <li><a href="/wasportalapps">WebSphere Portal</a></li>

              </ul>

            </div>
        </div>
        <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
            <ul className='nav-menu-items' onClick={showSidebar}> 
             <li className="navbar-toggle">
                 <Link to='#' className='menu-bars'>
                     <AiIcons.AiOutlineClose className="close" />

                 </Link>
             </li>
             {Sidebardata.map((item, index) => {

                 return (

                   <li key={index} className={item.cName}>
                       <Link to={item.path}>
                         
                           <span>{item.title}</span>
                       </Link>
                   </li>

                 )




             })}
            </ul>
          

        </nav>
        </>
    )
}

export default NavBar
