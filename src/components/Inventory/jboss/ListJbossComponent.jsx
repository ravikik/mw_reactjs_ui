import React, {useState} from 'react';
import {useEffect} from 'react';
import JbossService from '../../../services/JbossService';
import MaterialTable , {MTableToolbar,MTableHeader} from 'material-table';
import { tableIcons } from 'material-table';

import { handleRowUpdate } from 'material-table';
import { handleRowAdd } from 'material-table';
import { handleRowDelete } from 'material-table';
import { setErrorMessage } from 'material-table';








export default function ListJbossComponent() {

    var columns=[  
        {title:'APP Id',field:'appId',hidden: true, cellStyle: {
            width: 20,
            maxWidth: 20}},  
        {title:'Name',field:'appName'},
        {title:'Description',field:'appDesc'},
        {title:'Organization',field:'appOrg'},
        {title:'Vertical',field:'appVertical'},
        {title:'Contact',field:'appContact'},
        {title:'Alias',field:'appAlias'},
        {title:'Criticality',field:'appCrt'},
        {title:'Platform',field:'appPlt'},
        {title:'ContactDL',field:'appCDL'},
        {title:'Migration Phase',field:'appMigPhase'},
        {title:'Comments',field:'appComments'}
      ]

  /*  const [size, setSize] = React.useState(false);

    const handleTableOpen = () => {
        setSize(true);
      };
    
      const handleTableClose = () => {
        setSize(false);
      };*/
    


    const [data, setData] = useState([]);
    //for error handling
    const [iserror, setIserror] = useState(false)
    const [errorMessages, setErrorMessages] = useState([]) 

    useEffect(() => {
        JbossService.getJbossApp()
          .then(res => {
            setData(res.data)
          })
          .catch(error=>{
            setErrorMessage(["Cannot load user data"])
            setIserror(true)
          })
      }, [])


      
 


     


    return(<div>
                  
                    <MaterialTable title="Jboss Applications"

                    columns={columns}
                     data={data}
                     icons={tableIcons}
                     actions={[
                        {
                          icon: 'add',
                          tooltip: 'Add Application',
                          color: 'brown',
                          isFreeAction: true,
                          onClick: (event => window.location.href='/addAppl')
                        }
                      ]}
                     options={{
                         exportButton: true,
                         filtering: false,
                         draggable: true,
                         actionsColumnIndex: -1,
                         
                         
                         headerStyle: {
                            backgroundColor: '#2E3B55',
                            color: '#FFF',
                            fontSize: '15px',
                            
                            
                          },
                          rowStyle: {
                            backgroundColor: '#EEE',
                            fontSize: '12px'
                          },
                          pageSize: 10
                     }

                    }
                     
                     editable={{
                       onRowUpdate: (newData, oldData) =>
                         new Promise((resolve) => {
                           handleRowUpdate(newData, oldData, resolve);
                     }),

                    

                     onRowDelete: (oldData) =>
                       new Promise((resolve) => {
                         handleRowDelete(oldData, resolve)
                       }),
                     }}
                     />

    </div>

    )



   
}


