import React,{Component} from "react";
import { Form , Field } from "@progress/kendo-react-form";
import vertical from '../../../constants/Vertical';
import Organization from '../../../constants/Organization';




function AddJbossComponent() {

    const [ApplicationName, setApplicationName] = React.useState("");
    const [Organization, setOrganization] = React.useState("");
    const [BusinessCriticality, setBusinessCriticality] = React.useState("");
    const [Vertical, setVertical] = React.useState("");
    const [AppContact, setAppContact] = React.useState("");
    const [AppAlias, setAppAlias] = React.useState("");
    const [AppDescription, setAppDescription] = React.useState("");


    const handleSubmit = (event) => {
        console.log(`
          ApplicationName: ${ApplicationName}
          Organization: ${Organization}
          BusinessCriticality: ${BusinessCriticality}
          Vertical: ${Vertical}
          AppContact: ${AppContact}
          AppAlias: ${AppAlias}
          AppDescription: ${AppDescription}
        `);
    
        event.preventDefault();
      }

      return (
      <form onSubmit={handleSubmit}>
      <h1>Add Jboss Application</h1>

      <label>
        Application Name:
        <input
          name="ApplicationName"
          type="ApplicationName"
          value={ApplicationName}
          onChange={e => setApplicationName(e.target.value)}
          required />
      </label>

      <label>
        Organization:
        <select
          name="Organization"
          value={Organization}
          onChange={e => setOrganization(e.target.value)}
          required>
          <option key=""></option>
          {Organization.map(Organization => (
            <option key={Organization}>{Organization}</option>
          ))}
        </select>
      </label>

      <label>
       Business Criticality:
        <select
          name="BusinessCriticality"
          value={BusinessCriticality}
          onChange={e => setBusinessCriticality(e.target.value)}
          required />
 
      </label>

      <label>
        Vertical:
        <select
          name="Vertical"
          value={Vertical}
          onChange={e => setVertical(e.target.value)}
          required>
          <option key=""></option>
          {Vertical.map(Vertical => (
            <option key={Vertical}>{Vertical}</option>
          ))}
        </select>
      </label>

      <label>
       Application Contact:
        <select
          name="AppContact"
          value={Vertical}
          onChange={e => setAppContact(e.target.value)}
          required />

      </label>

      <label>
        Application Alias:
        <select
          name="AppAlias"
          value={AppAlias}
          onChange={e => setAppAlias(e.target.value)}
          required />
 
      </label>

      <label>
        Application Description:
        <select
          name="AppDescription"
          value={AppDescription}
          onChange={e => setAppDescription(e.target.value)}
          required />
 
      </label>




      <button>Submit</button>
    </form>
  );
}

export default AddJbossComponent
