import React, {useState} from 'react';
import {useEffect} from 'react';
import { handleRowAdd } from 'material-table';







const handleRowAdd = (newData, resolve) => {
  //validation
  let errorList = []
  if(newData.appName === undefined){
    errorList.push("Please enter Application name")
  }
  if(newData.appDesc === undefined){
    errorList.push("Please enter Application Description")
  }
  if(newData.email === undefined || validateEmail(newData.email) === false){
    errorList.push("Please enter a valid email")
  }
  if(errorList.length < 1){ //no error
    api.post("/users", newData)
      .then(res => {
        let dataToAdd = [...data];
        dataToAdd.push(newData);
        setData(dataToAdd);
        resolve()
        setErrorMessages([])
        setIserror(false)
     })
     .catch(error => {
        setErrorMessages(["Cannot add data. Server error!"])
        setIserror(true)
        resolve()
      })
  }else{
    setErrorMessages(errorList)
    setIserror(true)
    resolve()
  }
}

export default handleRowAdd;