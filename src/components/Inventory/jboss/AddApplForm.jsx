import React from 'react';
import {useForm} from "react-hook-form";

import JbossService from '../../../services/JbossService';




const AddApplForm = () => {
  const {register, handleSubmit, errors, reset} = useForm();
  const onSubmit = values => {JbossService.AddJbossAppl(values); reset()}
  //const onSubmit = values => console.log(values);
  console.log(errors);

  return (

<form className="form-style-9" onSubmit={handleSubmit(onSubmit)}>   

<h2 className="appHeader" >ADD APPLICATION </h2>
<ul>
<li>
    <input type="text" name="appName" className="field-style field-split align-left" placeholder="Application Name" ref={register({
          required: "Please provide the Application Name"
         
          })}/>
        {errors.appName && errors.appName.message} 
          
    <input type="text" name="appDesc" className="field-style field-split align-left" placeholder="Application Description" ref={register()}/>

</li>
<li>
    <input type="text" name="appOrg" className="field-style field-split align-left" placeholder="Organization" ref={register()}/>
    <input type="text" name="appVertical" className="field-style field-split align-left" placeholder="Vertical" ref={register()}/>

</li>
<li>
    <input type="text" name="appContact" className="field-style field-split align-left" placeholder="Application Contact" ref={register()}/>
    <input type="text" name="appCrt" className="field-style field-split align-left" placeholder="Application Criticality" ref={register()}/>

</li>
<li>
    <input type="text" name="appPlt" className="field-style field-split align-left" placeholder="Application Middleware Platform" ref={register()}/>
    <input type="text" name="appCDL" className="field-style field-split align-left" placeholder="Application contact DL" aria-invalid={errors.appCDL ? "true" : "false"} ref={register({
          required: "Required",
          pattern: {
            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
            message: "Invalid Email Address. Please provide a valid Email ID",
            
          }
        })}/>
    {errors.appCDL && errors.appCDL.message}        

</li>
<li>
<input type="text" name="appMigPhase" className="field-style field-full align-none" placeholder="Migration Phase" />
</li>
<li>
<textarea name="appComments" className="field-style" placeholder="Comments"></textarea>
</li>
<li>

<input type="submit" value="Submit" />

<input type="button" onClick={() => reset()} value="Reset" />
</li>
</ul>
</form>
    


 
 );
};


export default AddApplForm;
 









