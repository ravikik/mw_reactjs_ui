import React from "react";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import '../NavBar/NavBar.css';
import Button from "@material-ui/core/Button";

const drawerWidth = 180;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    
  },
  appBar: {
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
     
    })
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginRight: theme.spacing(2),
    color: "#f5f5f5"
  },
  hide: {
    display: "none"
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0
    
  },
  drawerPaper: {
    width: drawerWidth,
    background: "#2E3B55"
    
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end"
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    marginLeft: -drawerWidth
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    }),
    marginLeft: 0
  }
}));

export default function PersistentDrawerLeft() {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };




  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open
        })}
      >
        <Toolbar style={{background: '#2E3B55'}}>
          <IconButton
            
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, open && classes.hide)}
          >
            <MenuIcon />
          </IconButton>
          <div className="toolbar_logo">
            <a href="/">TMNA MIDDLEWARE INVENTORY</a>
          </div>
          <div className="spacer" />
          <div className="toolbar_navigation_lists">
            <ul>
            <li>
                <a href="/">Home</a>
              </li>
              <li>
                <a href="/getAllAppl">Jboss</a>
              </li>
              <li>
                <a href="/weblogicappls">WebLogic</a>
              </li>
              <li>
                <a href="/wasappls">WebSphere</a>
              </li>
              <li>
                <a href="/iisapps">IIS</a>
              </li>
              <li>
                <a href="/apacheurls">Apache</a>
              </li>
              <li>
                <a href="/wasportalapps">WebSphere Portal</a>
              </li>
            </ul>
          </div>
        </Toolbar>
      </AppBar>
      <Drawer
        
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        color="#2E3B55"
        open={open}
        classes={{
          paper: classes.drawerPaper
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "ltr" ? (
              <ChevronLeftIcon style={{color: '#f5f5f5'}} />
            ) : (
              <ChevronRightIcon />
            )}
          </IconButton>
        </div>
        <Divider />
 
          <div className="toolbar_sidebar_lists" >
            <ul>
            <li>
                <a href="/">Home</a>
              </li>
     
              <li>
                <a href="/getAllAppl">Jboss</a>
              </li>
              <li>
                <a href="/weblogicappls">WebLogic</a>
              </li>
              <li>
                <a href="/wasappls">WebSphere</a>
              </li>
              <li>
                <a href="/iisapps">IIS</a>
              </li>
              <li>
                <a href="/apacheurls">Apache</a>
              </li>
              <li>
                <a href="/wasportalapps">WebSphere Portal</a>
              </li>

            </ul>
          </div>

      </Drawer>
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: open
        })}
      >
        <div className={classes.drawerHeader} />
      </main>
    </div>
  );
}
