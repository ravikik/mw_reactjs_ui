
import * as AiIcons from 'react-icons/ai';
import React from 'react';
import * as IoIcons from 'react-icons/io';
import apacheimg from '../../icons/apache.jpg';
import jbossimg from '../../icons/jboss.png';
import weblogicimg from '../../icons/weblogic.jpg';
import wasimg from '../../icons/websphere.jpg';
import iisimg from '../../icons/IIS.png';
import webportalimg from '../../icons/webcenterportal.png';
import wasportalimg from '../../icons/wasportal.jpg';
import homeimg from '../../icons/home.png';



export const Sidebardata = [
   {
       title: 'Home',
       path: '/',
       icon: homeimg,
       cName: 'nav-text'
   },
 
   {
    title: 'Jboss',
    path: '/getAllAppl',
    icon: jbossimg,
    cName: 'nav-text'
   },
   {
    title: 'WebLogic',
    path: '/weblogicappls',
    icon: weblogicimg,
    cName: 'nav-text'
   },
   {
    title: 'WebSphere',
    path: '/wasappls',
    icon: wasimg,
    cName: 'nav-text'
   },
   {
    title: 'IIS',
    path: '/iisapps',
    icon: iisimg,
    cName: 'nav-text'
   },
   {
    title: 'Apache',
    path: '/apacheurls',
    icon: apacheimg,
    cName: 'nav-text'
   },
   {
    title: 'WebSphere Portal',
    path: '/wasportalapps',
    icon: wasportalimg,
    cName: 'nav-text'
   }


]



