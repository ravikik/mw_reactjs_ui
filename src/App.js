import React from 'react';
import { BrowserRouter as Router,Switch,Route} from 'react-router-dom';
import './App.css';
import AddApplForm from './components/Inventory/jboss/AddApplForm';

import ListJbossComponent from './components/Inventory/jboss/ListJbossComponent';
import NavBar  from './components/NavBar/NavBar';
import Sidebar from './components/Sidebar/Sidebar';



function App() {
  return (
    <>
      <Router>
      <Sidebar/>
        <Switch>
          <Route path="/addAppl"  exact component= {AddApplForm}  />
          <Route path="/getAllAppl" component = {ListJbossComponent}  />
          

        </Switch>

      </Router>
      </>
 
    
  );
}

export default App;
